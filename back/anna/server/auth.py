import functools

from flask_jwt_extended import jwt_required, get_jwt_identity

from anna.server.queries import get_user


def authorized(f):
    @functools.wraps(f)
    @jwt_required
    def decorated_function(*args, **kwargs):
        user_id = get_jwt_identity()
        user = get_user(user_id)
        return f(*args, **kwargs, user=user)
    return decorated_function