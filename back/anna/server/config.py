import os
from typing import cast

from peewee import PostgresqlDatabase


DB_HOST = os.getenv("DB_HOST")
DB_PORT = int(cast(str, os.getenv("DB_PORT")))
DB_USER = os.getenv("DB_USER")
DB_PASS = os.getenv("DB_PASS")
JWT_SECRET = os.getenv("JWT_SECRET")
PROD_ENV = os.getenv("PRODENV") in ["true", "True"]

db = PostgresqlDatabase(
    "anna",
    host=DB_HOST,
    port=int(DB_PORT),
    user=DB_USER,
    password=DB_PASS,
)
