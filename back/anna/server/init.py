from anna.server import models
from anna.server.config import db

INITIALIZED_MODELS = [
    models.User,
    models.Friend,
    models.Notification,
]

def init_tables():
    db.connect(reuse_if_open=True)
    db.create_tables(INITIALIZED_MODELS)
    db.close()
