import peewee as pw

from anna.server.config import db

class BaseModel(pw.Model):
    class Meta:
        database = db

class User(BaseModel):
    username = pw.CharField(
        unique=True,
        constraints=[pw.Check('length(username) > 0')]
    )
    password = pw.CharField()

class Friend(BaseModel):
    user = pw.ForeignKeyField(User, index=True)
    name = pw.CharField()
    note = pw.TextField()
    last_contact = pw.DateTimeField()

class Notification(BaseModel):
    user = pw.ForeignKeyField(User, index=True)
    due = pw.DateTimeField()
    msg = pw.TextField()
