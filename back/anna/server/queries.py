from datetime import datetime
from typing import Optional, List, Union

from anna.server.models import *

def register_user(username: str, password: str) -> int:
    """
    Registers the user and returns his id. If the username is already taken,
    raises an exception.
    """
    return User.create(username=username, password=password).id

def get_user(user_id: Union[str, int]) -> Optional[User]:
    """
    `user_id` can be either a user's username or id. If `user_id` is valid,
    fetches the user. Otherwise returns `None`.
    """
    try:
        if isinstance(user_id, str):
            return User.select().where(User.username == user_id).get()
        elif isinstance(user_id, int):
            return User.select().where(User.id == user_id).get()
        else:
            return None
    except User.DoesNotExist:
        return None

def create_friend(user: User, name: str, note: str) -> int:
    """
    Creates a friend for the given user. Returns the friend's id on success,
    `-1` otherwise.
    """
    try:
        return Friend.create(
            user=user,
            name=name,
            note=note,
            last_contact=datetime.fromtimestamp(0)
        ).id
    except:
        return -1

def get_friends(user: User) -> List[Friend]:
    """
    Returns all friends that the user has.
    """
    return list(Friend.select().where(Friend.user == user.id))

def update_friend(
    user: User,
    friend_id: int,
    name: Optional[str],
    note: Optional[str],
    last_contact: Optional[int]):
    """
    Updates a friend model. Returns nothing.
    """
    update_dict = {}
    if name is not None:
        update_dict["name"] = name
    if note is not None:
        update_dict["note"] = note
    if last_contact is not None:
        update_dict["last_contact"] = datetime.fromtimestamp(last_contact)

    if update_dict == {}:
        raise ValueError(
            "at least one of name, note, or last_contact must be set"
        )

    (Friend
        .update(update_dict)
        .where((Friend.id == friend_id) & (Friend.user == user.id))
        .execute()
    )

def delete_friend(user: User, friend_id: int) -> bool:
    """
    Deletes the specified friend. Returns `True` on success, `False` on failure.
    """
    return bool(Friend
        .delete()
        .where((Friend.id == friend_id) & (Friend.user == user.id))
        .execute()
    )

def create_notification(user: User, msg: str, due: int) -> int:
    """
    Creates the notification and returns its id.
    """
    return Notification.create(
        user=user,
        msg=msg,
        due=datetime.fromtimestamp(due)
    ).id

def get_notifications(user: User) -> List[Notification]:
    """
    Returns all of the user's notifications.
    """
    return list(Notification
        .select()
        .where(Notification.user == user.id)
        .execute()
    )
    
def delete_notification(user: User, notif_id: int) -> bool:
    """
    Deletes the notification. Returns `True` on success, `False` on failure.
    """
    return bool(Notification
        .delete()
        .where((Notification.id == notif_id) & (Notification.user == user.id))
        .execute()
    )
