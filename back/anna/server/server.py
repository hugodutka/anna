import datetime
import json
import traceback
from typing import Tuple

from flask import Flask, jsonify, request
from flask_cors import CORS
from flask_jwt_extended import (
    JWTManager, create_access_token, create_refresh_token, set_access_cookies,
    set_refresh_cookies, jwt_refresh_token_required, get_jwt_identity,
    unset_jwt_cookies
)
from werkzeug.security import generate_password_hash, check_password_hash

from anna.server import init, queries, models
from anna.server.config import db, JWT_SECRET, PROD_ENV
from anna.server.auth import authorized


def init_app() -> Tuple[Flask, JWTManager]:
    app = Flask(__name__)
    app.url_map.strict_slashes = False
    app.config['JWT_SECRET_KEY'] = JWT_SECRET
    app.config['JWT_TOKEN_LOCATION'] = ['cookies']
    app.config['JWT_ACCESS_COOKIE_PATH'] = '/'
    app.config['JWT_REFRESH_COOKIE_PATH'] = '/token/refresh'
    CORS(app, supports_credentials=True)
    jwt = JWTManager(app)
    init.init_tables()
    print("App initialized")
    return app, jwt

app, jwt = init_app()

@app.before_request
def connect_db():
    db.connect()

@app.teardown_request
def close_db(exception=None):
    if not db.is_closed():
        db.close()

@app.route("/login", methods=["POST"])
def login():
    data = request.get_json(silent=True)
    if data is None:
        return jsonify({"error": "not a json"}), 400

    username = data.get("username")
    password = data.get("password")
    user = queries.get_user(username)
    if user is None or not check_password_hash(user.password, password):
        return jsonify({"error": "invalid credentials"}), 400

    response = jsonify({"username": user.username, "id": user.id})
    access_token = create_access_token(
        identity=user.id, expires_delta=datetime.timedelta(days=3)
    )
    refresh_token = create_refresh_token(
        identity=user.id, expires_delta=datetime.timedelta(days=7)
    )
    set_access_cookies(response, access_token)
    set_refresh_cookies(response, refresh_token)

    response.set_cookie(
        "user_info",
        json.dumps({"id": user.id, "username": user.username}),
        secure=PROD_ENV,
        samesite="Strict"
    )

    return response

@app.route("/token/refresh", methods=["POST"])
@jwt_refresh_token_required
def refresh():
    current_user = get_jwt_identity()
    access_token = create_access_token(identity=current_user)
    resp = jsonify({"refresh": True})
    set_access_cookies(resp, access_token)
    return resp, 200

@app.route('/logout', methods=['POST'])
def logout():
    resp = jsonify({"logout": True})
    unset_jwt_cookies(resp)
    resp.set_cookie("user_info", expires=0)
    return resp, 200

@app.route("/user/create", methods=["POST"])
def register_user():
    data = request.json
    if not ("username" in data and "password" in data):
        return {"error": "malformed data"}, 400
    if len(data["username"]) < 1:
        return jsonify(
            {"error": "username must be at least 1 character long"}
        ), 400
    if len(data["password"]) < 3:
        return jsonify(
            {"error": "password must be at least 3 characters long"}
        ), 400
    try:
        user_id = queries.register_user(
            data["username"],
            generate_password_hash(data["password"])
        )
        return jsonify({"user_id": user_id}), 200
    except:
        traceback.print_exc()
        return jsonify({"error": "could not create the user"}), 500

@app.route('/identity', methods=["GET"])
@authorized
def identity(user: models.User):
    return jsonify({
        "authorized": True,
        "id": user.id,
        "username": user.username
    })

@app.route("/friend/create", methods=["POST"])
@authorized
def create_friend(user: models.User):
    data = request.json
    if not ("name" in data and "note" in data):
        return jsonify({"error": "malformed data"}), 400
    friend_id = queries.create_friend(user, data["name"], data["note"])
    if friend_id == -1:
        return jsonify({"error": "could not create the friend"}), 500
    else:
        return jsonify({"friend_id": friend_id}), 200

@app.route("/friend/get/all", methods=["GET"])
@authorized
def get_friends(user: models.User):
    friends = queries.get_friends(user)
    serialized_friends = [{
        "id": f.id,
        "nam": f.name,
        "not": f.note,
        "lc": int(datetime.datetime.timestamp(f.last_contact))
    } for f in friends]
    return jsonify({"friends": serialized_friends}), 200

@app.route("/friend/update", methods=["POST"])
@authorized
def update_friend(user: models.User):
    data = request.json
    if not "id" in data:
        return jsonify({"error": "malformed data"}), 400
    if (not "nam" in data) and (not "not" in data) and (not "lc" in data):
        return jsonify({"error": "malformed data"}), 400

    queries.update_friend(
        user=user,
        friend_id=data["id"],
        name=data.get("nam"),
        note=data.get("not"),
        last_contact=None if data.get("lc") is None else int(data.get("lc")),
    )

    return jsonify({"success": True}), 200

@app.route("/friend/delete", methods=["DELETE"])
@authorized
def delete_friend(user: models.User):
    data = request.json
    if not "id" in data:
        return jsonify({"error": "malformed data"}), 400

    if (queries.delete_friend(user, int(data["id"]))):
        return jsonify({"success": True})
    else:
        return jsonify({"error": "could not delete the user"}), 500

@app.route("/notification/create", methods=["POST"])
@authorized
def create_notification(user: models.User):
    data = request.json
    if not ("msg" in data and "due" in data):
        return jsonify({"error": "malformed data"}), 400
    notif_id = queries.create_notification(user, data["msg"], int(data["due"]))
    return jsonify({"id": notif_id}), 200

@app.route("/notification/get/all", methods=["GET"])
@authorized
def get_notifications(user: models.User):
    notifs = queries.get_notifications(user)
    serialized_notifs = [{
        "id": n.id,
        "msg": n.msg,
        "due": int(datetime.datetime.timestamp(n.due))
    } for n in notifs]
    return jsonify({"notifications": serialized_notifs}), 200

@app.route("/notification/delete", methods=["DELETE"])
@authorized
def delete_notification(user: models.User):
    data = request.json
    if not "id" in data:
        return jsonify({"error": "malformed data"}), 400

    if (queries.delete_notification(user, int(data["id"]))):
        return jsonify({"success": True})
    else:
        return jsonify({"error": "could not delete the notification"})
