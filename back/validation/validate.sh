export PGPASSWORD=testpass
psql \
    --host=localhost \
    --port 5432 \
    --username=postgres \
    -f validate.sql \
    --dbname=anna \
    --output=test.out
cmp --silent correct.out test.out && echo "test passed" || echo "test failed"
