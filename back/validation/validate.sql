SELECT id FROM friend
WHERE user_id NOT IN (
  SELECT id FROM user
);

SELECT id FROM notification
WHERE user_id NOT IN (
  SELECT id FROM user
);
