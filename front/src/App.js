import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import MoneyShot from '@/views/MoneyShot';
import Login from '@/containers/views/Login';
import Register from '@/containers/views/Register';
import NotFound from '@/views/NotFound';
import Friends from '@/containers/views/Friends';
import AppNavbar from '@/containers/components/AppNavbar';
import { HOME_URL, LOGIN_URL, REGISTER_URL } from '@/paths';
import './App.css';

const App = ({ loggedIn, userInitialized }) => {
  if (!userInitialized)
    return null;

  return <Router>
    <div id="app">
      <AppNavbar/>
      <Switch>
        <Route exact path={HOME_URL}>
          { loggedIn ? <Friends/> : <MoneyShot/> }
        </Route>
        <Route exact path={LOGIN_URL}>
          { loggedIn ? <Redirect to="/"/> : <Login/> }
        </Route>
        <Route exact path={REGISTER_URL}>
          { loggedIn ? <Redirect to="/"/> : <Register/> }
        </Route>
        <Route>
          <NotFound/>
        </Route>
      </Switch>
    </div>
  </Router>
};

export default App;
