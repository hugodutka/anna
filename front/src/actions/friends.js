import {
  fetchFriends,
  createFriend as fetchCreateFriend,
  deleteFriend as fetchDeleteFriend,
  updateFriend as fetchUpdateFriend,
} from '@/api'

export const ADD_UPDATE_FRIENDS = "ADD_UPDATE_FRIENDS";
export function addUpdateFriends(friends) {
  return { type: ADD_UPDATE_FRIENDS, friends };
}

export const FINISH_INIT_FRIENDS = "FINISH_INIT_FRIENDS";
export function finishInitFriends() {
  return { type: FINISH_INIT_FRIENDS };
}

export const FRIEND_MODAL_OPEN_CREATE = "FRIEND_MODAL_OPEN_CREATE";
export function openCreateModal() {
  return { type: FRIEND_MODAL_OPEN_CREATE };
}

export const FRIEND_MODAL_CLOSE = "FRIEND_MODAL_CLOSE";
export function closeModal() {
  return { type: FRIEND_MODAL_CLOSE };
}

export const FRIEND_MODAL_TYPE_NAME = "FRIEND_MODAL_TYPE_NAME";
export function typeNameModal(name) {
  return { type: FRIEND_MODAL_TYPE_NAME, name };
}

export const FRIEND_MODAL_TYPE_NOTE = "FRIEND_MODAL_TYPE_NOTE";
export function typeNoteModal(note) {
  return { type: FRIEND_MODAL_TYPE_NOTE, note };
}

export const FRIEND_START_SAVING = "FRIEND_START_SAVING";
export function startSaving() {
  return { type: FRIEND_START_SAVING };
}

export const FRIEND_FINISH_SAVING = "FRIEND_FINISH_SAVING";
export function finishSaving() {
  return { type: FRIEND_FINISH_SAVING };
}

export const CREATE_FRIEND = "CREATE_FRIEND";
export function createFriend(name, note) {
  return dispatch => {
    dispatch(startSaving());
    return fetchCreateFriend(name, note)
    .then(json => {
      if (typeof json.error !== 'undefined') {
        console.log("Create friend error", json.error);
        return dispatch(finishSaving());
      }
      var newFriends = {};
      newFriends[json.friend_id] = {
        id: json.friend_id,
        name: name,
        note: note,
        contact: 0,
      };
      dispatch(addUpdateFriends(newFriends));
      dispatch(finishSaving());
      dispatch(closeModal());
    });
  }
}

export const REMOVE_FRIEND_FROM_STATE = "REMOVE_FRIEND_FROM_STATE";
export function removeFriendFromState(id) {
  return { type: REMOVE_FRIEND_FROM_STATE, id };
}

export const DELETE_FRIEND = "DELETE_FRIEND";
export function deleteFriend(id) {
  return dispatch => {
    dispatch(startSaving());
    return fetchDeleteFriend(id)
    .then(json => {
      if (typeof json.error !== 'undefined') {
        console.log("Delete friend error", json.error);
        return dispatch(finishSaving());
      }
      dispatch(removeFriendFromState(id));
      dispatch(finishSaving());
    });
  }
}

export const INIT_FRIENDS = "INIT_FRIENDS";
export function initFriends() {
  return function action(dispatch) {
    return fetchFriends()
    .then(json => {
      if (typeof json.error !== 'undefined') {
        console.log("Fetch friends error", json.error);
        return;
      }
      const friends = json.friends.reduce((map, {id, nam, not, lc}) => {
        map[id] = {id, name: nam, note: not, contact: lc}
        return map;
      }, {});
      dispatch(addUpdateFriends(friends));
      dispatch(finishInitFriends());
    });
  }
}

export const CHECK_FRIEND_IN = "CHECK_FRIEND_IN";
export function checkFriendIn(id, name, note, prevContact) {
  return dispatch => {
    dispatch(startSaving());
    const now = Math.round(Date.now() / 1000);
    var updated = {};
    updated[id] = {id, name, note, contact: now};
    dispatch(addUpdateFriends(updated));
  
    return fetchUpdateFriend(id, name, note, now)
    .then(json => {
      if (typeof json.error !== 'undefined') {
        console.log("Check friend in error", json.error);
        var original = {};
        original[id] = {id, name, note, contact: prevContact};
        dispatch(addUpdateFriends(original));
        return dispatch(finishSaving());
      }
      dispatch(finishSaving());
    });
  }
}

export const FRIEND_MODAL_OPEN_EDIT = "FRIEND_MODAL_OPEN_EDIT";
export function openEditModal(id) {
  return { type: FRIEND_MODAL_OPEN_EDIT, id };
}

export const EDIT_FRIEND = "EDIT_FRIEND";
export function editFriend(id, name, note, contact) {
  return dispatch => {
    dispatch(startSaving());
    
    return fetchUpdateFriend(id, name, note, contact)
    .then(json => {
      if (typeof json.error !== 'undefined') {
        console.log("Check friend in error", json.error);
        return dispatch(finishSaving());
      }
      var edited = {};
      edited[id] = {id, name, note, contact};
      dispatch(addUpdateFriends(edited));
      dispatch(finishSaving());
      dispatch(closeModal());
    });
  }
}