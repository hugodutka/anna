import { login as fetchLogin } from '@/api'
import { setUserInfo } from '@/actions/user'

export const REQUEST_LOGIN = "REQUEST_LOGIN";
function requestLogin() {
  return { type: REQUEST_LOGIN };
}

export const FINISH_LOGIN = "FINISH_LOGIN";
function finishLogin(id, username) {
  return { type: FINISH_LOGIN, id, username };
}

export const FAIL_LOGIN = "FAIL_LOGIN";
function failLogin(reason) {
  return { type: FAIL_LOGIN, reason };
}

export const TYPE_USERNAME = "TYPE_USERNAME";
export function typeUsername(text) {
  return { type: TYPE_USERNAME, text };
}

export const TYPE_PASSWORD = "TYPE_PASSWORD";
export function typePassword(text) {
  return { type: TYPE_PASSWORD, text };
}

export function login(username, password) {
  return function action(dispatch) {
    dispatch(requestLogin());

    return fetchLogin(username, password)
    .then(json => {
      if (typeof json.error !== 'undefined') {
        console.log("Login error", json.error);
        dispatch(failLogin(json.error));
        return;
      }
      dispatch(finishLogin(json.id, json.username));
      dispatch(setUserInfo(json.username, json.id));
    });
  }
}
