import { resetState } from '@/actions'
import { initUser } from '@/actions/user'
import { logout as fetchLogout } from '@/api'

export const REQUEST_LOGOUT = 'REQUEST_LOGOUT';
const requestLogout = () => (
  { type: REQUEST_LOGOUT }
)

export const FAIL_LOGOUT = 'FAIL_LOGOUT';
const failLogout = () => (
  { type: FAIL_LOGOUT }
)

export const FINISH_LOGOUT = 'FINISH_LOGOUT';
const finishLogout = () => (
  { type: FINISH_LOGOUT }
)

export const LOGOUT = 'LOGOUT';
export function logout() {
  return function action(dispatch) {
    dispatch(requestLogout());

    return fetchLogout()
    .then(json => {
      if (typeof json.error !== 'undefined') {
        console.log("Logout error", json.error);
        return dispatch(failLogout(json.error));
      }
      dispatch(finishLogout(json.id, json.username));
      dispatch(resetState());
      dispatch(initUser());
    });
  }
}
