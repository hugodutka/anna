import {
  fetchNotifications,
  createNotification as fetchCreateNotification,
  deleteNotification as fetchDeleteNotification
} from '@/api'

export const OPEN_MODAL_NOTIF = "OPEN_MODAL_NOTIF";
export const openModal = () => (
  { type: OPEN_MODAL_NOTIF }
)

export const CLOSE_MODAL_NOTIF = "CLOSE_MODAL_NOTIF";
export const closeModal = () => (
  { type: CLOSE_MODAL_NOTIF }
)

export const CHOOSE_DATE_NOTIF = "CHOOSE_DATE_NOTIF";
export const chooseDate = (date) => (
  { type: CHOOSE_DATE_NOTIF, date }
)

export const TYPE_NAME_NOTIF = "TYPE_NAME_NOTIF";
export const typeName = (name) => (
  { type: TYPE_NAME_NOTIF, name }
)

export const ADD_UPDATE_NOTIFS = "ADD_UPDATE_NOTIFS";
export const addUpdateNotifications = (notifications) => (
  { type: ADD_UPDATE_NOTIFS, notifications }
)

export const REQUEST_INIT_NOTIF = "REQUEST_INIT_NOTIF";
export const requestInitNotifications = () => (
  { type: REQUEST_INIT_NOTIF }
)

export function initNotifications() {
  return function action(dispatch) {
    dispatch(requestInitNotifications());
    return fetchNotifications()
    .then(json => {
      if (typeof json.error !== 'undefined') {
        console.log("Fetch friends error", json.error);
        return;
      }
      const notifs = json.notifications.reduce((map, {id, msg, due}) => {
        map[id] = {id, msg, due: new Date(due * 1000)}
        return map;
      }, {});
      dispatch(addUpdateNotifications(notifs));
      dispatch(finishInitNotifications());
    });
  }
}

export const FINISH_INIT_NOTIF = "FINISH_INIT_NOTIF";
export const finishInitNotifications = () => (
  { type: FINISH_INIT_NOTIF }
)

export const REQUEST_CREATE_NOTIF = "REQUEST_CREATE_NOTIF";
const requestCreateNotification = () => (
  { type: REQUEST_CREATE_NOTIF }
)

export const FINISH_CREATE_NOTIF = "FINISH_CREATE_NOTIF";
const finishCreateNotification = () => (
  { type: FINISH_CREATE_NOTIF }
)

export function createNotification(msg, due) {
  return function action(dispatch) {
    dispatch(requestCreateNotification());
    return fetchCreateNotification(msg, due)
    .then(json => {
      if (typeof json.error !== 'undefined') {
        console.log("Fetch friends error", json.error);
        dispatch(finishCreateNotification());
        return;
      }
      var notifs = {};
      notifs[json.id] = {id: json.id, msg, due: new Date(due * 1000)};
      dispatch(addUpdateNotifications(notifs));
      dispatch(finishCreateNotification());
      dispatch(closeModal());
    });
  }
}

export const REMOVE_NOTIFICATION_FROM_STATE = "REMOVE_NOTIFICATION_FROM_STATE";
export function removeNotificationFromState(id) {
  return { type: REMOVE_NOTIFICATION_FROM_STATE, id };
}

export function deleteNotification(id, msg, due) {
  return dispatch => {
    dispatch(removeNotificationFromState(id));
    return fetchDeleteNotification(id)
    .then(json => {
      if (typeof json.error !== 'undefined') {
        console.log("Delete notification error", json.error);
        var notif = {};
        notif[id] = {id, msg, due};
        dispatch(addUpdateNotifications(notif));
      }
    });
  }
}
