import { register as fetchRegister } from '@/api'
import { login } from '@/actions/login'

export const REQUEST_REGISTER = "REQUEST_REGISTER";
function requestRegister() {
  return { type: REQUEST_REGISTER };
}

export const FAIL_REGISTER = "FAIL_REGISTER";
function failRegister(reason) {
  return { type: FAIL_REGISTER, reason };
}

export const TYPE_REGISTER_USERNAME = "TYPE_REGISTER_USERNAME";
export function typeRegisterUsername(text) {
  return { type: TYPE_REGISTER_USERNAME, text };
}

export const TYPE_REGISTER_PASSWORD = "TYPE_REGISTER_PASSWORD";
export function typeRegisterPassword(text) {
  return { type: TYPE_REGISTER_PASSWORD, text };
}

export function register(username, password) {
  return function action(dispatch) {
    dispatch(requestRegister());

    return fetchRegister(username, password)
    .then(json => {
      if (typeof json.error !== 'undefined') {
        console.log("register error", json.error);
        dispatch(failRegister(json.error));
        return;
      }
      dispatch(login(username, password));
    });
  }
}
