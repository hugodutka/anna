import { identity as fetchIdentity } from '@/api'
import Cookies from 'js-cookie'

export const SET_USER_INFO = "SET_USER_INFO";
export function setUserInfo(username, id) {
  return { type: SET_USER_INFO, username, id };
}

export const FINISH_INIT_USER = "FINISH_INIT_USER";
export function finishInitUser() {
  return { type: FINISH_INIT_USER };
}

export const INIT_USER = "INIT_USER";
export function initUser() {
  return function action(dispatch) {
    const user_info = Cookies.get("user_info");
    if (typeof(user_info) === 'undefined') {
      dispatch(finishInitUser());
    } else {
      return fetchIdentity()
      .then(json => {
        if (json.authorized)
          dispatch(setUserInfo(json.username, json.id));
        dispatch(finishInitUser());
      });
    }
  }
}
