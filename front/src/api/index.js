import { APIHost } from './config.js';
import Cookies from 'js-cookie';


const fetchAPI = (endpoint, method, data) => {
  const url = APIHost + endpoint;
  var options = {
    method: method,
    credentials: 'include',
    mode: 'cors',
    headers: {
      'X-CSRF-TOKEN': Cookies.get('csrf_access_token'),
      'Content-Type': 'application/json',
    },
  };
  if (method === "GET") {
    options.data = data;
  } else if (method === "POST") {
    options.body = JSON.stringify(data);
  } else if (method === "DELETE") {
    options.body = JSON.stringify(data);
  } else {
    console.error(`fetchAPI: method ${method} not implemented`);
  }
  return fetch(url, options)
  .then(
    response => response.json()
      .catch(e => {
        console.log("fetch error", response, e);
        return { error: e.toString() };
      }),
    error => ({ error: error.toString() })
  );
}

export const login = (username, password) => {
  return fetchAPI("/login", "POST", {username, password});
}

export const register = (username, password) => {
  return fetchAPI("/user/create", "POST", {username, password});
}

export const logout = () => {
  return fetchAPI("/logout", "POST", {});
}

export const identity = () => {
  return fetchAPI("/identity", "GET", {});
}

export const fetchFriends = () => {
  return fetchAPI("/friend/get/all", "GET", {});
}

export const createFriend = (name, note) => {
  return fetchAPI("/friend/create", "POST", {name, note});
}

export const deleteFriend = (id) => {
  return fetchAPI("/friend/delete", "DELETE", {id});
}

export const updateFriend = (id, name, note, contact) => {
  return fetchAPI("/friend/update", "POST", {
    id,
    nam: name,
    not: note,
    lc: contact
  });
}

export const fetchNotifications = () => {
  return fetchAPI("/notification/get/all", "GET", {});
}

export const createNotification = (msg, due) => {
  return fetchAPI("/notification/create", "POST", {msg, due});
}

export const deleteNotification = (id) => {
  return fetchAPI("/notification/delete", "DELETE", {id});
}
