import React from "react";
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import Button from 'react-bootstrap/Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faRocket, faUser } from '@fortawesome/free-solid-svg-icons'
import { Link } from 'react-router-dom';
import { HOME_URL, LOGIN_URL, REGISTER_URL } from '@/paths';
import NotificationList from '@/containers/components/NotificationList'
import NotificationModal from '@/containers/components/NotificationModal'
import "./index.css";

export const AppNavbar = ({ username, atLogout }) => (
  <Navbar className="app-navbar">
    <Link to={HOME_URL}>
      <Navbar.Brand>
        <FontAwesomeIcon icon={faRocket} className="navbar-brand-icon"/>
        Anna
      </Navbar.Brand>
    </Link>
    <Navbar.Toggle aria-controls="basic-navbar-nav" />
    <Navbar.Collapse id="basic-navbar-nav">
      <Nav className="mr-auto"></Nav>
      {
        username === null ?
        <>
          <Nav className="mr-sm-2 navbar-buttons">
            <Link to={REGISTER_URL}>
              <Button variant="success">Register</Button>
            </Link>
            <Link to={LOGIN_URL}>
              <Button variant="outline-secondary">Login</Button>
            </Link>
          </Nav>
        </>
        :
        <>
          <Nav className="mr-sm-2">
            <Nav.Item>
              <Nav.Link disabled className="navbar-username">
                <FontAwesomeIcon icon={faUser} className="navbar-user-icon"/>
                {username}
              </Nav.Link>
            </Nav.Item>
            <span className="navbar-buttons">
              <NotificationList/>
              <NotificationModal/>
              <Button
                variant="outline-secondary"
                onClick={atLogout} >
                  Logout
              </Button>
            </span>
          </Nav>
        </>
      }
    </Navbar.Collapse>
  </Navbar>
);

export default AppNavbar;
