import React from "react"
import Button from 'react-bootstrap/Button'
import Card from 'react-bootstrap/Card'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPen, faTrash, faCheck } from '@fortawesome/free-solid-svg-icons'
import { dateToText } from '@/utils'
import "./index.css"

export const FriendCard = (
  {id, name, note, contact, onDelete, onEdit, onCheck}
) => (
  <Card className="friendcard">
    <Card.Body>
      <Card.Title>
        <div className="friendcard-header">
          <span className="friendcard-name">
            {name}
          </span>
          <span className="friendcard-buttons">
            <Button variant="outline-secondary"
                    onClick={() => onDelete(id)}>
              <FontAwesomeIcon icon={faTrash}/>
            </Button>
            <Button variant="outline-primary"
                    onClick={() => onEdit(id)}>
              <FontAwesomeIcon icon={faPen}/>
            </Button>
            <Button variant="outline-success"
                    onClick={() => onCheck(id, name, note, contact)}>
              <FontAwesomeIcon icon={faCheck}/>
            </Button>
          </span>
        </div>
      </Card.Title>
      <Card.Text>
        {note}
      </Card.Text>
      <Card.Text>
        <span className="friendcard-date">
          Checked in: {contact === 0 ? "Never" : dateToText(contact * 1000)}
        </span>
      </Card.Text>
    </Card.Body>
  </Card>
)

export default FriendCard;