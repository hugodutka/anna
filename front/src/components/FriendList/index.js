import React from "react"
import Alert from 'react-bootstrap/Alert'
import { FriendCard } from '@/components/FriendCard'
import './index.css'

export const FriendList = (
  {friends, deleteFriend, checkFriendIn, openEditModal}
) => {
  if (friends.length === 0)
    return <Alert variant="primary">You haven't added any friends yet.</Alert>

  return <div className="friends-list">
    {friends.map(({id, name, note, contact}) => (
      <FriendCard
        id={id}
        name={name}
        note={note}
        contact={contact}
        onDelete={deleteFriend}
        onCheck={checkFriendIn}
        onEdit={openEditModal}
        key={id}
      />
    ))}
  </div>
};

export default FriendList;