import React from "react"
import Button from 'react-bootstrap/Button'
import Modal from 'react-bootstrap/Modal'
import Form from 'react-bootstrap/Form'
import Spinner from 'react-bootstrap/Spinner'
import { createFriend, editFriend } from '@/actions/friends'
import "./index.css"

export const FriendModal = (
  {name, note, onNameChange, onNoteChange, show, onHide, dispatch, id, origName,
   isSaving, contact}
) => (
  <Modal
    show={show}
    onHide={onHide}
    size="lg"
    aria-labelledby="contained-modal-title-vcenter"
    animation={false}
    centered
  >
    <Modal.Header closeButton>
      <Modal.Title>
        {id === null ? "New friend" : `Editing: ${origName}`}
      </Modal.Title>
    </Modal.Header>
    <Modal.Body>
      <Form>
        <Form.Group>
          <Form.Control type="text" placeholder="Name"
            onChange={onNameChange} value={name}/>
        </Form.Group>
        <Form.Group>
          <Form.Control as="textarea" rows="8" placeholder="Note"
            onChange={onNoteChange} value={note}/>
        </Form.Group>
      </Form>
      <Button variant="success" className="friend-modal-savebtn"
        disabled={isSaving}
        onClick={id === null ?
          () => dispatch(createFriend(name, note)) :
          () => dispatch(editFriend(id, name, note, contact))
        }
      >
        { isSaving ? <Spinner/> : "Save" }
      </Button>
    </Modal.Body>
  </Modal>
);

export default FriendModal;