import React from 'react'
import Badge from 'react-bootstrap/Badge'
import Button from 'react-bootstrap/Button'
import Popover from 'react-bootstrap/Popover'
import OverlayTrigger from 'react-bootstrap/OverlayTrigger'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheck, faPlus } from '@fortawesome/free-solid-svg-icons'
import { faBell } from '@fortawesome/free-regular-svg-icons'
import { dateToText } from '@/utils'
import './index.css'

export const NotificationList = (
  {notifications, initNotifications, isInit, isInitStarted, overdue, openModal,
  onDelete}
) => {
  if (!isInit && !isInitStarted)
    initNotifications();
  
  const popover = (
    <Popover className="notif-popover" id="notif-popover">
      <Popover.Title className="notif-list-header">
        <b>Reminders</b>
        <Button
          variant="outline-dark"
          className="notif-add-btn"
          onClick={openModal}
        >
          <FontAwesomeIcon icon={faPlus}/>
        </Button>
      </Popover.Title>
      <Popover.Content>
        {[...notifications.keys()].map(i => {
          const {id, msg, due, isOverdue} = notifications[i];
          return <div key={id}>
            <div className="notif">
              <div>
                <b>{msg}</b>
                <br/>
                <span className="notif-date">{dateToText(due)}</span>
              </div>
              <div>
                <Button
                  className="notif-trash-btn"
                  variant={isOverdue ? "success" : "outline-dark"}
                  onClick={() => onDelete(id)}
                >
                  <FontAwesomeIcon icon={faCheck}/>
                </Button>
              </div>
            </div>
            {i + 1 !== notifications.length && <hr className="notif-hr"/>}
          </div>
        })}
      </Popover.Content>
    </Popover>
  );

  return <OverlayTrigger trigger="click" placement="bottom" overlay={popover}>
    <Button variant="outline-secondary">
      <FontAwesomeIcon icon={faBell}/>
      {overdue > 0 && <>&nbsp;<Badge variant="danger">{overdue}</Badge></>}
    </Button>
  </OverlayTrigger>;
};

export default NotificationList;