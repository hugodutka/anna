import React from 'react'
import Button from 'react-bootstrap/Button'
import Modal from 'react-bootstrap/Modal'
import Form from 'react-bootstrap/Form'
import DayPicker from 'react-day-picker'
import 'react-day-picker/lib/style.css'
import './index.css'

export const NotificationModal = (
  {name, date, isOpen, isSaving, closeModal, chooseDate, typeName, onSave}
) => (
  <Modal
    show={isOpen}
    onHide={closeModal}
    size="sm"
    aria-labelledby="contained-modal-title-vcenter"
    animation={false}
    centered
  >
    <Modal.Header closeButton>
      <Modal.Title>New Reminder</Modal.Title>
    </Modal.Header>
    <Modal.Body>
      <div className="notif-modal-group">
        <Form.Control
          type="textfield"
          placeholder="Reminder"
          value={name}
          onChange={typeName}  
        />
        <DayPicker
          className="notif-daypicker"
          onDayClick={chooseDate}
          selectedDays={date}
        />
        <Button variant="success" className="friend-modal-savebtn"
          onClick={() => onSave(name, Math.round(date.getTime() / 1000))}
          disabled={isSaving}
        >
          Save
        </Button>
      </div>
    </Modal.Body>
  </Modal>
);

export default NotificationModal;