import { connect } from 'react-redux'
import App from '@/App'

const mapStateToProps = state => ({
  loggedIn: state.user.id !== null,
  userInitialized: state.user.initialized,
})

export default connect(mapStateToProps)(App)
