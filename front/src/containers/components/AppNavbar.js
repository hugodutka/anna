import { connect } from 'react-redux'
import AppNavbar from '@/components/AppNavbar'
import { logout } from '@/actions/logout'

const mapStateToProps = state => ({
  username: state.user.username,
})

const mapDispatchToProps = dispatch => ({
  atLogout: () => dispatch(logout()),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppNavbar)
