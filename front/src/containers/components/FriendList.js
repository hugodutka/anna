import { connect } from 'react-redux'
import {
  deleteFriend, checkFriendIn, openEditModal
} from '@/actions/friends'
import FriendList from '@/components/FriendList'

const mapStateToProps = state => ({
  friends: Object.values(state.friends.friends)
           .sort((a, b) => a.contact - b.contact),
})

const mapDispatchToProps = dispatch => ({
  deleteFriend: id => dispatch(deleteFriend(id)),
  checkFriendIn: (id, name, note, prevContact) => dispatch(
    checkFriendIn(id, name, note, prevContact)
  ),
  openEditModal: id => dispatch(openEditModal(id)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(FriendList)
