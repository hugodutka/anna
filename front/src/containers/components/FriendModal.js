import { connect } from 'react-redux'
import {
  closeModal, typeNameModal, typeNoteModal
} from '@/actions/friends'
import FriendModal from '@/components/FriendModal'

const mapStateToProps = state => {
  const f = state.friends;
  return {
    name: f.modalName,
    note: f.modalNote,
    contact: f.editedId === null ? null : f.friends[f.editedId].contact, 
    show: f.isBeingCreated || f.editedId !== null,
    id: f.editedId,
    origName: f.editedName,
    isSaving: f.isSaving,
  };
}

const mapDispatchToProps = dispatch => ({
  onNameChange: (e) => dispatch(typeNameModal(e.target.value)),
  onNoteChange: (e) => dispatch(typeNoteModal(e.target.value)),
  onHide: () => dispatch(closeModal()),
  dispatch,
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FriendModal)
