import { connect } from 'react-redux'
import {
  openModal, initNotifications, deleteNotification
} from '@/actions/notifications'
import NotificationList from '@/components/NotificationList'

const mapStateToProps = state => {
  const notifications = Object.values(state.notifications.notifications)
    .map(notif => {
      const due = new Date(notif.due);
      due.setHours(0,0,0,0);
      return {...notif, isOverdue: due.getTime() <= new Date(Date.now())};
    })
    .sort((a, b) => a.due - b.due);

  return {
    isInit: state.notifications.isInit,
    isInitStarted: state.notifications.isInitStarted,
    overdue: notifications.reduce(
      ((n, {isOverdue}) => n += isOverdue ? 1 : 0), 0
    ),
    notifications,
  };
}

const mapDispatchToProps = dispatch => ({
  openModal: () => dispatch(openModal()),
  initNotifications: () => dispatch(initNotifications()),
  onDelete: (id, msg, due) => dispatch(deleteNotification(id, msg, due)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(NotificationList)
