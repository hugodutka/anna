import { connect } from 'react-redux'
import {
  closeModal, chooseDate, typeName, createNotification
} from '@/actions/notifications'
import NotificationModal from '@/components/NotificationModal'

const mapStateToProps = state => ({
  isOpen: state.notifications.modalOpen,
  isSaving: state.notifications.isSaving,
  name: state.notifications.formName,
  date: state.notifications.formDate,
})

const mapDispatchToProps = dispatch => ({
  closeModal: () => dispatch(closeModal()),
  chooseDate: date => dispatch(chooseDate(date)),
  typeName: e => { e.preventDefault(); dispatch(typeName(e.target.value)); },
  onSave: (msg, due) => dispatch(createNotification(msg, due)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(NotificationModal)
