import { connect } from 'react-redux'
import Friends from '@/views/Friends'
import { initFriends, openCreateModal} from '@/actions/friends'


const mapStateToProps = state => ({
  isInitialized: state.friends.isInitialized,
})

const mapDispatchToProps = dispatch => ({
  initFriends: () => dispatch(initFriends()),
  openCreateModal: () => dispatch(openCreateModal()),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Friends)