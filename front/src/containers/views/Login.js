import { connect } from 'react-redux'
import { login, typeUsername, typePassword } from '@/actions/login'
import Login from '@/views/Login'


const mapStateToProps = state => ({
  username: state.login.formUsername,
  password: state.login.formPassword,
  failMsg: state.login.failMsg,
})

const mapDispatchToProps = dispatch => ({
  onSubmit: (username, password) => dispatch(login(username, password)),
  onUsernameChange: e => dispatch(typeUsername(e.target.value)),
  onPasswordChange: e => dispatch(typePassword(e.target.value)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login)