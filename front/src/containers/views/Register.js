import { connect } from 'react-redux'
import {
  register, typeRegisterUsername, typeRegisterPassword
} from '@/actions/register'
import Register from '@/views/Register'


const mapStateToProps = state => ({
  username: state.register.formUsername,
  password: state.register.formPassword,
  failMsg: state.register.failMsg,
})

const mapDispatchToProps = dispatch => ({
  onSubmit: (username, password) => dispatch(register(username, password)),
  onUsernameChange: e => dispatch(typeRegisterUsername(e.target.value)),
  onPasswordChange: e => dispatch(typeRegisterPassword(e.target.value)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Register)