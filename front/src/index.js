import React from 'react';
import thunkMiddleware from 'redux-thunk'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import rootReducer from './reducers'
import './index.css';
import App from './containers/App';
import { initUser } from './actions/user'
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';

const composeEnhancers = composeWithDevTools({});
export const store = createStore(
  rootReducer,
  composeEnhancers(
    applyMiddleware(thunkMiddleware)
  )
)

store.dispatch(initUser());

render(
  <Provider store={store}>
    <App/>
  </Provider>,
  document.getElementById('root')
)

serviceWorker.unregister();
