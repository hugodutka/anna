import {
  ADD_UPDATE_FRIENDS, FINISH_INIT_FRIENDS, INIT_FRIENDS,
  FRIEND_MODAL_OPEN_CREATE, FRIEND_MODAL_CLOSE, FRIEND_MODAL_TYPE_NAME,
  FRIEND_MODAL_TYPE_NOTE, REMOVE_FRIEND_FROM_STATE, FRIEND_MODAL_OPEN_EDIT,
} from '@/actions/friends'

const initialState = {
  isInitialized: false,
  isSaving: false,
  friends: {},
  isBeingCreated: false,
  modalName: null,
  modalNote: null,
  editedId: null,
  editedName: null,
}

export function friends(state = initialState, action) {
  switch (action.type) {
    case ADD_UPDATE_FRIENDS:
      return {
        ...state,
        friends: {
          ...state.friends,
          ...action.friends
        }
      };
    case FINISH_INIT_FRIENDS:
      return {
        ...state,
        isInitialized: true,
      };
    case INIT_FRIENDS:
      return {
        ...state,
        isInitialized: false,
      };
    case FRIEND_MODAL_OPEN_CREATE:
      return {
        ...state,
        isBeingCreated: true,
        modalName: "",
        modalNote: "",
      };
    case FRIEND_MODAL_CLOSE:
      return {
        ...state,
        isBeingCreated: false,
        editedId: null,
      };
    case FRIEND_MODAL_TYPE_NAME:
      return {
        ...state,
        modalName: action.name,
      };
    case FRIEND_MODAL_TYPE_NOTE:
      return {
        ...state,
        modalNote: action.note,
      };
    case REMOVE_FRIEND_FROM_STATE:
      var newState = {
        ...state,
        friends: {...state.friends} 
      };
      delete newState.friends[action.id];
      return newState;
    case FRIEND_MODAL_OPEN_EDIT:
      return {
        ...state,
        editedId: action.id,
        editedName: state.friends[action.id].name,
        modalName: state.friends[action.id].name,
        modalNote: state.friends[action.id].note,
      };
    default:
      return state;
  }
}

export default friends;