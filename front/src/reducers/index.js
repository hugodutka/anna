import { combineReducers } from 'redux'
import { RESET_STATE } from '@/actions'
import { login } from './login'
import { register } from './register'
import { logout } from './logout'
import { user } from './user'
import { friends } from './friends'
import { notifications } from './notifications'

const appReducer = combineReducers({
  login,
  logout,
  user,
  friends,
  register,
  notifications,
});

const rootReducer = (state, action) => {
  if (action.type === RESET_STATE) {
    return { ...state, ...appReducer(undefined, action) }
  }
  return appReducer(state, action)
}

export default rootReducer;
