import {
  REQUEST_LOGIN, FINISH_LOGIN, FAIL_LOGIN, TYPE_PASSWORD, TYPE_USERNAME
} from '@/actions/login';

const initialState = {
  formUsername: "",
  formPassword: "",
  isLoggingIn: false,
  failMsg: null
}

export function login(state = initialState, action) {
  switch (action.type) {
    case REQUEST_LOGIN:
      return {
        ...state,
        isLoggingIn: true,
        failMsg: null
      };
    case FAIL_LOGIN:
      return {
        ...state,
        isLoggingIn: false,
        failMsg: action.reason,
      };
    case FINISH_LOGIN:
      return initialState;
    case TYPE_USERNAME:
      return {
        ...state,
        formUsername: action.text,
      };
    case TYPE_PASSWORD:
      return {
        ...state,
        formPassword: action.text,
      };
    default:
      return state;
  }
}

export default login;
