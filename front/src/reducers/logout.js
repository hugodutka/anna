import {
  REQUEST_LOGOUT, FINISH_LOGOUT, FAIL_LOGOUT, LOGOUT
} from '@/actions/logout'

const initialState = {
  isLoggingOut: false,
  fail: false
}

export function logout(state = initialState, action) {
  switch (action.type) {
    case REQUEST_LOGOUT:
      return {
        ...state,
        isLoggingOut: true,
        fail: false,
      };
    case FINISH_LOGOUT:
      return {
        ...state,
        isLoggingOut: false,
        fail: false,
      };
    case FAIL_LOGOUT:
      return {
        ...state,
        isLoggingOut: false,
        fail: true,
      };
    case LOGOUT:
      return state;
    default:
      return state;
  }
}

export default logout;