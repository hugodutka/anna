import {
  OPEN_MODAL_NOTIF, CLOSE_MODAL_NOTIF, CHOOSE_DATE_NOTIF, TYPE_NAME_NOTIF,
  ADD_UPDATE_NOTIFS, FINISH_INIT_NOTIF, REQUEST_INIT_NOTIF,
  REQUEST_CREATE_NOTIF, FINISH_CREATE_NOTIF, REMOVE_NOTIFICATION_FROM_STATE
} from '@/actions/notifications'

const initialState = {
  isInit: false,
  isInitStarted: false,
  isSaving: false,
  listOpen: false,
  modalOpen: false,
  formName: "",
  formDate: null,
  notifications: {},
}

export function notifications(state = initialState, action) {
  switch (action.type) {
    case OPEN_MODAL_NOTIF:
      return {
        ...state,
        modalOpen: true,
        formName: "",
      };
    case CLOSE_MODAL_NOTIF:
      return {
        ...state,
        modalOpen: false,
      };
    case CHOOSE_DATE_NOTIF:
      return {
        ...state,
        formDate: action.date,
      };
    case TYPE_NAME_NOTIF:
      return {
        ...state,
        formName: action.name,
      };
    case ADD_UPDATE_NOTIFS:
      return {
        ...state,
        notifications: {
          ...state.notifications,
          ...action.notifications
        }
      };
    case REQUEST_INIT_NOTIF:
      return {
        ...state,
        isInitStarted: true
      };
    case FINISH_INIT_NOTIF:
      return {
        ...state,
        isInit: true,
      };
    case REQUEST_CREATE_NOTIF:
      return {
        ...state,
        isSaving: true,
      };
    case FINISH_CREATE_NOTIF:
      return {
        ...state,
        isSaving: false,
      };
    case REMOVE_NOTIFICATION_FROM_STATE:
      var newState = {
        ...state,
        notifications: {...state.notifications} 
      };
      delete newState.notifications[action.id];
      return newState;
    default:
      return state;
  }
}

export default notifications;