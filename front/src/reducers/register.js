import {
  REQUEST_REGISTER, FAIL_REGISTER, TYPE_REGISTER_PASSWORD,
  TYPE_REGISTER_USERNAME
} from '@/actions/register';

const initialState = {
  formUsername: "",
  formPassword: "",
  isRegistering: false,
  failMsg: null
}

export function register(state = initialState, action) {
  switch (action.type) {
    case REQUEST_REGISTER:
      return {
        ...state,
        isRegistering: true,
        failMsg: null
      };
    case FAIL_REGISTER:
      return {
        ...state,
        isRegistering: false,
        failMsg: action.reason,
      };
    case TYPE_REGISTER_USERNAME:
      return {
        ...state,
        formUsername: action.text,
      };
    case TYPE_REGISTER_PASSWORD:
      return {
        ...state,
        formPassword: action.text,
      };
    default:
      return state;
  }
}

export default register;
