import { SET_USER_INFO, INIT_USER, FINISH_INIT_USER } from '@/actions/user'

const initialState = {
  username: null,
  id: null,
  initialized: false,
}

export function user(state = initialState, action) {
  switch (action.type) {
    case SET_USER_INFO:
      return {
        ...state,
        username: action.username,
        id: action.id
      };
    case INIT_USER:
      return state;
    case FINISH_INIT_USER:
      return {
        ...state,
        initialized: true
      };
    default:
      return state;
  }
}

export default user;