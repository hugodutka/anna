const dayLength = 24 * 3600 * 1000;

export const dateToText = date => {
  date = new Date(date);
  date = Date.UTC(date.getFullYear(), date.getMonth(), date.getDate());
  var today = new Date(Date.now())
  today = Date.UTC(today.getFullYear(), today.getMonth(), today.getDate());
  const diff = date - today;
  const days = diff / dayLength;

  if (days < -1) {
    return `${Math.abs(days)} days ago`;
  } else if (days < 0) {
    return "Yesterday";
  } else if (days < 1) {
    return "Today";
  } else if (days < 2) {
    return "Tomorrow";
  } else {
    return `In ${days} days`;
  }
}
