import React from "react"
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Button from 'react-bootstrap/Button'
import FriendList from '@/containers/components/FriendList'
import FriendModal from '@/containers/components/FriendModal'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlus } from '@fortawesome/free-solid-svg-icons'
import "./index.css"

export const Friends = ({isInitialized, initFriends, openCreateModal}) => {
  if (!isInitialized)
    initFriends();
  
  return <div className="friends-view bg-light">
    <Container fluid>
      <Row>
        <Col lg={2}></Col>
        <Col lg={8}>
          <br/>
          <div className="friends-header">
            <h1 className="friends-title">Friends</h1>
            <Button variant="success" className="friends-addbutton"
              onClick={openCreateModal}>
              <FontAwesomeIcon icon={faPlus}/>&nbsp; Add a friend
            </Button>
          </div>
          <br/>
          {isInitialized && <FriendList/>}
        </Col>
        <Col lg={2}></Col>
      </Row>
    </Container>

    <FriendModal/>
  </div>
}

export default Friends;