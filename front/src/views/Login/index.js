import React from "react";
import Card from 'react-bootstrap/Card';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Alert from 'react-bootstrap/Alert';
import "./index.css";

export default function Login({
  onSubmit, onUsernameChange, onPasswordChange, username, password, failMsg
}) {
  return (
    <React.Fragment>
      <div className="login-view bg-light">
        <Card className="login-box">
          <Card.Body>
            <h3>Login</h3>
            <hr/>
            <Form onSubmit={
              e => { e.preventDefault(); onSubmit(username, password); }
            }>
              <Form.Group controlId="formBasicEmail">
                <Form.Control
                  type="text"
                  name="username"
                  placeholder="Username"
                  onChange={onUsernameChange}
                  value={username}
                />
              </Form.Group>
              <Form.Group controlId="formBasicPassword">
                <Form.Control
                  type="password"
                  placeholder="Password"
                  onChange={onPasswordChange}
                  value={password}
                />
              </Form.Group>
              {
                failMsg === null ? 
                  null :
                  <Alert variant="danger">Error: {failMsg}</Alert>
              }
              <Button
                variant="primary"
                type="submit"
                className="login-submit-button"
              >
                Submit
              </Button>
            </Form>
          </Card.Body>
        </Card>
      </div>
    </React.Fragment>
  )
}
