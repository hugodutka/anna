import React from "react";
import Button from 'react-bootstrap/Button';
import { Link } from 'react-router-dom';
import rocket_launch from './rocket_launch.svg';
import { REGISTER_URL } from '@/paths';
import "./index.css";

export default function MoneyShot(props) {
  return (
    <React.Fragment>
      <div
        className="moneyshot-view"
        style={{backgroundImage: `url(${rocket_launch})`}}
      >
        <div className="moneyshot-box">
          <h1 className="moneyshot-title">Anna</h1>
          <h3>Friend Relationship Management</h3>
          <br/>
          <Link to={REGISTER_URL}>
            <Button variant="success" className="moneyshot-signup-button">
              <b>Sign up for Anna</b>
            </Button>
          </Link>
        </div>
      </div>
    </React.Fragment>
  )
}