import React from "react";
import notfoundImg from './404.svg';
import "./index.css";

export default function NotFound(props) {
  return (
    <React.Fragment>
      <div className="notfound-view">
        <img className="notfound-img" src={notfoundImg} alt="Page not found"/>
      </div>
    </React.Fragment>
  )
}