import React from "react";
import Card from 'react-bootstrap/Card';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Alert from 'react-bootstrap/Alert';
import "./index.css";

export const Register = ({
  onSubmit, onUsernameChange, onPasswordChange, username, password, failMsg
}) => (
  <div className="register-view bg-light">
    <Card className="register-box">
      <Card.Body>
        <h3>Register</h3>
        <hr/>
        <Form onSubmit={
          e => { e.preventDefault(); onSubmit(username, password); }
        }>
          <Form.Group controlId="formBasicEmail">
            <Form.Control
              type="username"
              placeholder="Username"
              onChange={onUsernameChange}
              value={username}
            />
          </Form.Group>
          <Form.Group controlId="formBasicPassword">
            <Form.Control
              type="password"
              placeholder="Password"
              onChange={onPasswordChange}
              value={password}
            />
          </Form.Group>
          {
            failMsg === null ? 
              null :
              <Alert variant="danger">Error: {failMsg}</Alert>
          }
          <Button
            variant="success"
            type="submit"
            className="register-submit-button"
          >
            Sign up
          </Button>
        </Form>
      </Card.Body>
    </Card>
  </div>
);

export default Register;
